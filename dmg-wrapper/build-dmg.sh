#!/bin/bash
set -euo pipefail # Unofficial strict mode, see http://redsymbol.net/articles/unofficial-bash-strict-mode/

cd "$(dirname "$(readlink -f "$0")")"
# see https://stackoverflow.com/questions/3349105/how-can-i-set-the-current-working-directory-to-the-directory-of-the-script-in-ba

INTELJRETAR="OpenJDK11U-jre_x64_mac_hotspot_11.0.15_10.tar.gz"
APPLEJRETAR="OpenJDK17U-jre_aarch64_mac_hotspot_17.0.2_8.tar.gz"

APP="SheepIt"

BASE="$(pwd)"
BUILD="$BASE/build"
BIN="$BASE/bin"
SH_DIR="$BUILD/SheepIt.app/Contents/MacOS"
RES="$BUILD/SheepIt.app/Contents/Resources"
CL="$RES/client"

function extract-jre() {
    JRETAR=${1:?}
    tar -axf "$BASE/$JRETAR" -C "$RES"  # Extracts the JRE into Resources folder
    cd "$CL"
    ln -s "$(find ../ -name java)" $APP  # We symlink java to make the title appearing in the dock to read "SheepIt", not "Java"
    cd "$BASE"
    echo "Extracted JRE $JRETAR"
}

function add-jar() {
    cp "$BUILD/$APP.jar" "$CL"
    echo "Added $APP jar"
}

function build-dmg() {
    NAME=${1:?}
    DMG="$NAME-raw.dmg"
    FINAL="$NAME.dmg"
    genisoimage -quiet -D -V $APP -no-pad -r -apple -o "$DMG" "$BUILD"
    ./dmg "$DMG" "$BIN/$FINAL"  # Use libdmg based dmg tool to compress the dmg (and also make it read-only)
    rm "$DMG"
    echo "Built $FINAL under $BIN, grabbable via \"docker cp\"."
}

rm -rf "$BUILD" "$BIN" || true # The "|| true" is that we expect the command to fail (If there is nothing to remove), needed since we are running in strict mode
mkdir -p "$BUILD"
mkdir -p "$BIN"
tar -axf "$APP.tar.gz" -C "$BUILD"  # Extract base file structure

mkdir -p "$SH_DIR"
cp "$BASE/$APP.sh" "$SH_DIR/$APP" # Shell exec shim
mkdir -p "$CL"
cp "$BASE/$APP-Run.sh" "$CL"
cp "$BASE/$APP.png" "$CL"

build-dmg "$APP-Slim"

touch "$CL/DEBUG"
build-dmg "$APP-Slim-DEBUG"

wget https://www.sheepit-renderfarm.com/media/applet/client-latest.php -O "$BUILD/$APP.jar"  # Copy client artifact to be packaged

ARCH="Intel-x64"
extract-jre $INTELJRETAR
build-dmg "$APP-$ARCH-Lite"
add-jar
build-dmg "$APP-$ARCH-Full"

rm -rf "${CL:?}/$APP" "$CL/$APP.jar" "$RES/jdk"*
echo "Cleaned up JRE & Jar"

ARCH="Apple-ARM"
extract-jre $APPLEJRETAR
build-dmg "$APP-$ARCH-Lite"
add-jar
build-dmg "$APP-$ARCH-Full"

echo "======DONE======"
echo "All DMG's generated!"
echo "located under $BIN"
echo "Grab via \"docker cp dmg-wrapper:$BIN <targetdir>\""
rm -rf "$BUILD"
